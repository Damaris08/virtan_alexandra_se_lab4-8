package edu.tucn.se.lab9.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
public class Win extends JFrame {
 Container c;
 JButton but;
 TextField textf;
 public Win(String name) {
 super(name);
 init();
 setVisible(true);
 setDefaultCloseOperation(EXIT_ON_CLOSE);
 
 }
 public void init()
 {
 this.setBounds(300, 200, 500, 500);
 this.setBackground(Color.CYAN);
 this.c = getContentPane();
 this.c.setLayout(null);
 this.c.setBackground(Color.DARK_GRAY);
 
 but = new JButton("Click!");
 but.setBounds(350,350,100,100);
 but.addActionListener(new ClickHandeler());
 but.setForeground(Color.magenta);
 but.setOpaque(true);
 
 c.add(but);
 
 textf = new TextField();
 textf.setBounds(100,100,60,60);
 textf.setText("0");
 textf.setEditable(false);
 textf.setBackground(Color.BLACK);
 textf.setForeground(Color.WHITE);
 c.add(textf);
 
 }
 
 class ClickHandeler implements ActionListener{
 int nrClicks= 0;
 public void actionPerformed(ActionEvent e){
 nrClicks++;
 Win.this.textf.setText(Integer.toString(nrClicks));
 }
 }

 
}