package edu.tucn.se.lab10.ex5;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        Buffer b = new Buffer();
        Producer pro = new Producer(b);
        Consumer c = new Consumer(b);
        Consumer c2 = new Consumer(b);
        pro.start();
        c.start();
        c2.start();

        Thread.sleep(1000 * 5); 

        c2.stopTh();
        c.stopTh();
        while(c2.isAlive() || c.isAlive()) { 
            Thread.sleep(1000);
        }
        pro.stopTh();
    }
}
class Producer implements Runnable {
    private Buffer bf;
    private Thread thread;
    private boolean running = true;

    Producer(Buffer bf) { 
        this.bf = bf;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this); 
            thread.start();
        }
    }
    public void run() {
        while (running) {
            bf.push(Math.random());
            System.out.println("Am scris.");
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }
    }
    public void stopTh() {
        System.out.println("Closing " + this);
        running = false;
    }
}
class Consumer extends Thread {
    private Buffer bf;
    private boolean running = true;

    Consumer(Buffer bf) { 
        this.bf = bf;
    }

    public void run() {
        while (running) { 
            System.out.println("Am citit : " + this + " >> " + bf.get());
        }
    }

    public void stopTh(){  
        System.out.println("Closing " + this);
        running = false;
    }
}

class Buffer {
    ArrayList<Double> content = new ArrayList<>();
    synchronized void push(double d) {
        content.add(d);
        notify(); 
    }
    synchronized double get() {
        double d = -1;
        try {
            while (content.size() == 0) wait(); 
            d = (Double) content.get(0);  
            content.remove(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
}