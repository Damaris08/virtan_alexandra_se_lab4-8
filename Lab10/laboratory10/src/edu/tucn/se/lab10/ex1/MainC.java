package edu.tucn.se.lab10.ex1;
import edu.tucn.se.lab10.ex1.Counter;
public class MainC {
	   public static void main(String[] args) {
           Counter c1 = new Counter("counter1");
           Counter c2 = new Counter("counter2");
           Counter c3 = new Counter("counter3");
           c1.run();
           c2.start();
           c3.start();
}
}
