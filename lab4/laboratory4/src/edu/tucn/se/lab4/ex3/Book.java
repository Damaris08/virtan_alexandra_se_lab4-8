package edu.tucn.se.lab4.ex3;

public class Book {
 private String name;
 private edu.tucn.se.lab4.ex2.Author author;
 private double price;
 private int qtyInStock = 0;
 
 public Book(String name, edu.tucn.se.lab4.ex2.Author author, double price)
 {
	 this.name=name;
	 this.author=author;
	 this.price=price;
     
 }
 public Book(String name, edu.tucn.se.lab4.ex2.Author author, double price , int qtyInStock)
 {
	 this.name=name;
	 this.author=author;
	 this.price=price;
	 this.qtyInStock=0;
 }

public String getName()
{
	return name;
}
public edu.tucn.se.lab4.ex2.Author getAuthor()
{
	return this.author;
}

public double getPrice()
{
	return price;
}

public int getQtyInStock()
{
	return qtyInStock;
}
public void setPrice (double price)
{
	this.price=price;
}
public void setQtyInStock (int qtyInStock)
{
	this.qtyInStock=qtyInStock;
}


public void ToString() {
    System.out.println("'" + this.getName() + "' by ");
    author.ToString();
    System.out.println("\r\n");

}

}
