package edu.tucn.se.lab4.ex6;

public class Square extends Rectangle{
	public Square()
	{
		super();
		
	}
  public Square(double side)
  {
	  super(side, side);
  }
  public Square(double side, String color,boolean filled)
  {
	  super(side,side,color,filled);
  }
  public double getSide()
  {
	  return super.getWidth();
  }
  public void setSide(double side)
  {
	  super.setWidth(side);
  }
@Override

public void setLength(double length) {
    super.length = super.width = length;
}

public void setWidth(double width) {
    super.width = super.length = width;
}
 
 public String toString()
 {
	 return "A square with side"+super.length+"which is a subclass of"+super.toString();
 }
}
