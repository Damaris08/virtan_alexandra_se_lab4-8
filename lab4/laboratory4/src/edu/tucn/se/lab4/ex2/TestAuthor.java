package edu.tucn.se.lab4.ex2;

public class TestAuthor {
    public static void main(String[] args) {
        ///Three authors + functions + print
        Author Alin = new Author("Alin", "g@mail", 'm');
        Author Ionut = new Author("Ionut", "y@mail", 'm');
        Author Adina = new Author("Adina", "la@mail", 'f');

        System.out.println("The initial values : \r\n");
        Alin.ToString();
        Ionut.ToString();
        Adina.ToString();

        System.out.println("The changed values : \r\n");

        Alin.setEmail("alinbogdan@gmail.com");
        Ionut.setGender('m');
        Adina.setName("Karla");

        Alin.ToString();
        Ionut.ToString();
        Adina.ToString();
    }
}
