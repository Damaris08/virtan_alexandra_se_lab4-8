package edu.tucn.se.lab4.ex2;
//import java.awt.datatransfer.SystemFlavorMap

 

public class Author {
    private String name;
    private String email;
    char gender;

 

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        if (gender == 'm' || gender == 'f') {
            this.gender = gender;
        } else {
            System.out.println("Invalid gender! ");
        }
    }

 

    public void setName(String name) {
        this.name = name;
    }

 

    public String getName() {
        return this.name;
    }

 

    public void setEmail(String email) {
        this.email = email;
    }

 

    public String getEmail() {
        return this.email;
    }

 

    public void setGender(char gender) {
        if (gender == 'm' || gender == 'f') this.gender = gender;
        else  System.out.println("Invalid gender! ");
    }

 

    public char getGender() {
        return this.gender;
    }
    
    public void ToString()
    {
        System.out.println(this.getName()+" ("+this.getGender()+")"+" at "+this.getEmail()+"\r\n");
    }
}