package edu.tucn.se.lab4.ex4;
import edu.tucn.se.lab4.ex2.Author;
import java.util.Scanner;
public class TestBook {
	public static void main (String[] args)
	{
	 int aut_length;
	 
			Scanner scan = new Scanner(System.in);
			Author [] authors = new Author [] {
						   new Author("Paulo Cohelo", "pauloc@gmail.com", 'm'),
						   new Author("Ioan Slavici", "ioans@gmail.com", 'm'),
						   new Author("Jane Austen", "janea@gmail.com", 'f') };
			 aut_length = 2;
			 
			 for(int i=0;i<= aut_length;i++)
				 
			 {
				 System.out.println("Name:");
				 String name = scan.next();
				 System.out.println("Email:");
				 String email= scan.next();
				 System.out.println("Gender:");
				 char gender = scan.next().charAt(0);
				 authors[i] = new Author(name, email, gender);
				
			 }
	  
	  Book b = new Book("Enigma Otiliei", authors, 35, 150);
	  b.toString();
	  b.printAuthors();
	  
	} 
	  	
}
