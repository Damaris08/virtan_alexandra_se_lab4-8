package edu.tucn.se.lab6.ex3;
import edu.tucn.se.lab6.ex1.BankAccount;
import edu.tucn.se.lab6.ex2.SortByBalance;
import edu.tucn.se.lab6.ex2.SortByOwner;
import java.util.TreeSet;

 

public class TestBTreeSet {
    public static void main(String[] args) {
        TreeSet<BankAccount> tb = new TreeSet<BankAccount>(new SortByBalance());
        TreeSet<BankAccount> to = new TreeSet<BankAccount>(new SortByOwner());
        BankAccount ba1 = new BankAccount("Andrei", 24);
        BankAccount ba2 = new BankAccount("Ioana", 8);
        BankAccount ba3 = new BankAccount("Damaris", 23);
        BankAccount ba4 = new BankAccount("Crina", 50);
        BankAccount ba5 = new BankAccount("Paul", 56);
        BankAccount ba6 = new BankAccount("Alina", 2921);
        tb.add(ba1);
        tb.add(ba2);
        tb.add(ba3);
        tb.add(ba4);
        tb.add(ba5);
        tb.add(ba6);

 

        to.add(ba1);
        to.add(ba2);
        to.add(ba3);
        to.add(ba4);
        to.add(ba5);
        to.add(ba6);

 

        BankTreeSet vara = new BankTreeSet(tb,to);
        vara.printAccountsByBalance();
        vara.printAccountsByOwners();

 

        vara.addAccount("Sammi",234);
        System.out.println("Array with Sammi added sorted by Owner's name alphabetically : \r\n");
        vara.printAccountsByOwners();

 

        System.out.println("\r\nGive Damaris Account: ");
        BankAccount cont = vara.getAccount("Dan");
        System.out.println("Damaris account is :"+cont.toString());

 

        System.out.println("\r\nGive all accounts between 100 and 3000:");
        vara.printAccounts(100,3000);
    }
}
