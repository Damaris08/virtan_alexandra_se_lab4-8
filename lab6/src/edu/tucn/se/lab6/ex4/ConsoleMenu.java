package edu.tucn.se.lab6.ex4;

public class ConsoleMenu {
    public static void main(String[] args) {
    
        StoreWord st = new StoreWord(new Word(" Clasa"), new Definition(" Colectie de obiecte "));
        Dictionary dict = new Dictionary(st);
        dict.addWord(new Word(" Obiect"), new Definition(" Instanta a unei clase "));
        dict.addWord(new Word(" OOP"), new Definition(" Un concept in programrare"));
        dict.addWord(new Word(" Constructor"), new Definition("Functie publica pentru a creea un obiect/instanta"));
        dict.addWord(new Word(" C++"), new Definition(" Limbaj de programare"));

        System.out.println("The words are:");
        dict.getAllWords();
        System.out.println("\r\n");

        System.out.println("The definitions are:");
        dict.getAllDefinitions();
        System.out.println("\r\n");

        Word wo = new Word("SQL");
        dict.addWord(wo, new Definition("Limbaj Pentru baze de date"));
        
        System.out.println("The SQL description :" + dict.getDefinition(wo).toString());
    }
}