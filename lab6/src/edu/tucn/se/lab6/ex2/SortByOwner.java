package edu.tucn.se.lab6.ex2;

import edu.tucn.se.lab6.ex1.BankAccount;

import java.util.Comparator;

public class SortByOwner implements Comparator<BankAccount> {

    public int compare(BankAccount a, BankAccount b)
    {
       if (a.getOwner().compareTo(b.getOwner())>0) return 1;
       if (a.getOwner().compareTo(b.getOwner())==0) return 0;
       return -1;

    }

}