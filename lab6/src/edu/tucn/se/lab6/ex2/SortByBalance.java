package edu.tucn.se.lab6.ex2;

import edu.tucn.se.lab6.ex1.BankAccount;

import java.util.Comparator;

public class SortByBalance implements Comparator<BankAccount> 
{
    public int compare(BankAccount a, BankAccount b) 
    {
        return (int) (a.getBalance() - b.getBalance());
    }

}