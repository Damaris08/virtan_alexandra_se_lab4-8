package edu.tucn.se.lab6.ex2;

import edu.tucn.se.lab6.ex1.BankAccount;

import java.util.ArrayList;

public class TestB{
    public static void main(String[] args) 
    {
        ArrayList<BankAccount> baarray = new ArrayList<BankAccount>();
        BankAccount ba1 = new BankAccount("Andrei", 24);
        BankAccount ba2 = new BankAccount("Ioana", 8);
        BankAccount ba3 = new BankAccount("Damaris", 23);
        BankAccount ba4 = new BankAccount("Crina", 50);
        BankAccount ba5 = new BankAccount("Paul", 56);
        BankAccount ba6 = new BankAccount("Alina", 2921);
        baarray.add(ba1);
        baarray.add(ba2);
        baarray.add(ba3);
        baarray.add(ba4);
        baarray.add(ba5);
        baarray.add(ba6);
        Bank var = new Bank(baarray);
        System.out.println("Sorted array: \r\n");
        var.printAccounts();

        var.addAccount("Sammi",234);
        System.out.println("Array with Iosif added : \r\n");
        var.printAccounts();

        System.out.println("\r\nGive Damaris Account: ");
        BankAccount cont = var.getAccount("Dan");
        System.out.println("Damaris account is :"+cont.toString());

        System.out.println("\r\nGive all accounts between 100 and 3000:");
        var.printAccounts(100,3000);

        System.out.println("\r\nGive all accounts alphabetically:");
        var.getALllAccounts();



    }
}