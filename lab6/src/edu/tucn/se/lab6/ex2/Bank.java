package edu.tucn.se.lab6.ex2;
import edu.tucn.se.lab6.ex1.BankAccount;
import java.util.ArrayList;
import java.util.Collections;

public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

    public Bank(ArrayList<BankAccount> acc) 
    {
        this.accounts = acc;
    }
    public void addAccount(String Owner, double Balance) 
    {
        BankAccount anew = new BankAccount(Owner, Balance);
        accounts.add(anew);
    }

    public void printAccounts() 
    {
        System.out.println("Unsorted: ");
        for (BankAccount ba : accounts)
            System.out.println(ba.toString());
        System.out.println("\r\n Sorted: ");
        Collections.sort(accounts, new SortByBalance());
        for (BankAccount ba : accounts)
            System.out.println(ba.toString());

    }
    public void printAccounts(double minBalance, double maxBalance)
    {
        for (BankAccount ba : accounts) {
            if (ba.getBalance() > minBalance && ba.getBalance() < maxBalance) System.out.println(ba.toString());
        }
    }

   
    public BankAccount getAccount(String own) 
    {
        for (BankAccount ba : accounts) {
            if (ba.getOwner() == own) return ba;
        }
        return new BankAccount("NU EXISTA CONT",0);
    }
    public void getALllAccounts()
    {
        System.out.println("\r\n Sorted Alphabetically: ");
        Collections.sort(accounts, new SortByOwner());
        for (BankAccount ba : accounts)
            System.out.println(ba.toString());
    }
    
}