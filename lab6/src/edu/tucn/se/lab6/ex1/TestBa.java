package edu.tucn.se.lab6.ex1;

public class TestBa {
	public static void main(String[] args)
	{
        BankAccount ba1 = new BankAccount("Mara", 8);
        BankAccount ba2 = new BankAccount("Ionela", 65746);
        BankAccount ba3 = new BankAccount("Mara", 8);
        System.out.println("\r\n");
        System.out.println("ba1 == ba2 ? : " + ba1.equals(ba2));
        System.out.println("ba1 hashcode : " + ba1.hashCode() + " ba2 hashcode : " + ba2.hashCode() + "\r\n");
        System.out.println("ba1 == ba3 ? : " + ba1.equals(ba3));
        System.out.println("ba1 hashcode : " + ba1.hashCode() + " ba3 hashcode : " + ba3.hashCode() + "\r\n");
    }
	
	}

