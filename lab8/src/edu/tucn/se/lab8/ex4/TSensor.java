package edu.tucn.se.lab8.ex4;

import java.util.Random;

public class TSensor {
    private CoolingSystem cools;
    private HeatingSystem heats;
    public int temperature;
    private int id;
    public TSensor(int Id, CoolingSystem c, HeatingSystem h) {
        this.id = Id;
        this.cools = c;
        this.heats = h;
    }

    public CoolingSystem getCooling() {
        return this.cools;
    }

    public HeatingSystem getHeats() {
        return this.heats;
    }

}
