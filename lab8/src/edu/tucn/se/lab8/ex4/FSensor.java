package edu.tucn.se.lab8.ex4;

public class FSensor {
    private int id;
    private GsmUnit gsm;
    private Alarm alarm;
    private Boolean smoke;
    FSensor(int id, GsmUnit g, Alarm a)
    {
        this.id=id;
        this.gsm=g;
        this.alarm=a;
    }

    public Alarm GetAlarms(){return this.alarm;}
    public GsmUnit GetGsm(){return this.gsm;}
    public int GetId(){return this.id;}
    public Boolean GetSmoke(){return this.smoke;}
    public void SetSmoke(Boolean b){this.smoke=b;}
}
