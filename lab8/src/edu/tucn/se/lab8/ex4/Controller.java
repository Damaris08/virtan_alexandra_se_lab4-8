package edu.tucn.se.lab8.ex4;
import java.util.ArrayList;
import java.util.Random;
public class Controller {
	private static Controller ContUnit=null;
	private TSensor TemperatureSensor;
    private ArrayList<FSensor> fireList = new ArrayList<FSensor>();
    private Random r = new Random();
    private Controller() {
        TemperatureSensor = new TSensor(11, new CoolingSystem(), new HeatingSystem());
        FSensor f1 = new FSensor(0, new GsmUnit(), new Alarm());
        FSensor f2 = new FSensor(1, new GsmUnit(), new Alarm());
        FSensor f3 = new FSensor(2, new GsmUnit(), new Alarm());
        FSensor f4 = new FSensor(3, new GsmUnit(), new Alarm());
        fireList.add(f1);
        fireList.add(f2);
        fireList.add(f3);
        fireList.add(f4);
    }

    public static Controller getController() {
        if (ContUnit != null) return ContUnit;
        else return new Controller();
    }

    public void control(int event) {
        if (event < 20) {
            TemperatureSensor.temperature = r.nextInt(60);
            System.out.println("The temperature is: "+TemperatureSensor.temperature);
            if (TemperatureSensor.temperature < 25) {
                TemperatureSensor.getHeats().tooCold();
            }
            if (TemperatureSensor.temperature > 40) {
                TemperatureSensor.getCooling().tooHot();
            }
            if(TemperatureSensor.temperature>=25 && TemperatureSensor.temperature<=40){
                System.out.println("The temperature is optimal!");
            }

            System.out.println("\r\n");

        }

        if (event >= 20 && event <= 40) {

            int location = r.nextInt(3);
            fireList.get(location).SetSmoke(r.nextBoolean());

            if (fireList.get(location).GetSmoke() == true) {
                System.out.println("Smoke detected in area covered by the sensor with id" + fireList.get(location).GetId());
                fireList.get(location).GetAlarms().alarmFire();
                fireList.get(location).GetGsm().callOwner();
            } else {
                System.out.println("There is no significant smoke detected (false alarm)!");

            }
            System.out.println("\r\n");


        }
        if (event > 40) {
            System.out.println("The house is OK!");
            System.out.println("\r\n");
        }


    }
}
