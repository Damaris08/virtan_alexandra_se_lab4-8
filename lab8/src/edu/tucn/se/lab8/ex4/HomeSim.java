package edu.tucn.se.lab8.ex4;
import java.util.Random;
public class HomeSim {
	
	 public static void main(String[] args) {
	        Random sim = new Random();
	        int steps = 15;
	        int event;
	        while (steps > 0) {
	            event = sim.nextInt(60);
	            Controller.getController().control(event);
	            try {
	                Thread.sleep(300);
	            } catch (InterruptedException ex) {
	                ex.printStackTrace();
	            }
	            steps--;
	        }


	    }
}
