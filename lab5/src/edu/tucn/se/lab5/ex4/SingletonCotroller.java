package edu.tucn.se.lab5.ex4;

import edu.tucn.se.lab5.ex3.LightSensor;
import edu.tucn.se.lab5.ex3.TemperatureSensor;

public class SingletonController {
    private TemperatureSensor tmps;
    private LightSensor ls;
    private static SingletonController scl;
    private SingletonController(TemperatureSensor tmpsen, LightSensor lsen) {
        tmps = tmpsen;
        ls = lsen;
    }
    public void control() {
        int i = 1;
        while (i <= 20) {
            tmps.getValue();
            System.out.println("Temperature location " + tmps.getLocation() + " second " + i + ":" + tmps.readValue());
            ls.getValue();
            System.out.println("Light location " + ls.getLocation() + " second " + i + ":" + ls.readValue());
            System.out.println("\r\n");
            i++;
        }
    }
    public static SingletonController getController(){
        if (scl==null) {
            TemperatureSensor tmpsen = new TemperatureSensor("Kitchen");
            LightSensor lsen = new LightSensor ("Kitchen");
            scl=new SingletonController(tmpsen,lsen);
        }
        return scl;
    }


}
