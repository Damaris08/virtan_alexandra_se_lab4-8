package edu.tucn.se.lab5.ex2;

public class ProxyImage implements Image{
	 
	   private RealImage realImage;
	   private String fileName;
	 
	   public ProxyImage(String fileName,char p){
	      this.fileName = fileName;
	      if (p=='R') {this.RotatedImage();}
	      if (p=='P') {this.display();}
	      if (p!='R' && p!='P') {System.out.println("Wrong Parameter!");}
	   }
	 
	   @Override
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();
	   }
	   
	   @Override
	    public void RotatedImage() {
	        if (realImage == null){
	            realImage=new RealImage(fileName);
	        }
	        realImage.RotatedImage();
	    }
	}