package edu.tucn.se.lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
	private int value;
	public TemperatureSensor(String location)
	{
		super(location);
	}
  public void getValue()
  {
	  Random rand=new Random();
	  this.value=rand.nextInt(100);
  }
  public int readValue()
  {
	  return this.value;
  }
}
